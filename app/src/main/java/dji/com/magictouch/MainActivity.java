package dji.com.magictouch;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.atap.tangoservice.Tango;
import com.google.atap.tangoservice.TangoConfig;
import com.google.atap.tangoservice.TangoCoordinateFramePair;
import com.google.atap.tangoservice.TangoErrorException;
import com.google.atap.tangoservice.TangoEvent;
import com.google.atap.tangoservice.TangoOutOfDateException;
import com.google.atap.tangoservice.TangoPoseData;
import com.google.atap.tangoservice.TangoXyzIjData;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button control, camera, video;
    private TextView locationData;

    private Tango mTango;
    private TangoConfig mConfig;

    private static final String TAG = MainActivity.class.getSimpleName();

    ArrayList<float[]> tangoPose = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        control = (Button) findViewById(R.id.control);
        camera = (Button) findViewById(R.id.camera);
        video = (Button) findViewById(R.id.video);

        locationData = (TextView) findViewById(R.id.locationData);

        control.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mTango = new Tango(getApplicationContext());
                    mConfig = new TangoConfig();
                    mConfig = mTango.getConfig(TangoConfig.CONFIG_TYPE_CURRENT);
                    mConfig.putBoolean(TangoConfig.KEY_BOOLEAN_MOTIONTRACKING, true);
                    mConfig.putBoolean(TangoConfig.KEY_BOOLEAN_DEPTH, true);

                    final ArrayList<TangoCoordinateFramePair> framePairs =
                            new ArrayList<TangoCoordinateFramePair>();
                    framePairs.add(new TangoCoordinateFramePair(
                            TangoPoseData.COORDINATE_FRAME_START_OF_SERVICE,
                            TangoPoseData.COORDINATE_FRAME_DEVICE));

                    mTango.connectListener(framePairs, new Tango.OnTangoUpdateListener() {
                        @Override
                        public void onPoseAvailable(TangoPoseData tangoPoseData) {
                            logPose(tangoPoseData);
                            tangoPose.add(tangoPoseData.getTranslationAsFloats());
//                            for (TangoCoordinateFramePair i: framePairs) {
//                                locationData.setText(mTango.getPoseAtTime(tangoPoseData.timestamp, i).toString());
//                            }
                        }

                        @Override
                        public void onXyzIjAvailable(TangoXyzIjData tangoXyzIjData) {

                        }

                        @Override
                        public void onFrameAvailable(int i) {

                        }

                        @Override
                        public void onTangoEvent(TangoEvent tangoEvent) {

                        }
                    });


                    try {
                        mTango.connect(mConfig);
                    } catch (TangoOutOfDateException e) {
                        // handle the error
                    }

                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    try {
                        mTango.disconnect();
                    } catch (TangoErrorException e) {
                        // handle the error
                    }

                }
                return false;
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    private void logPose(TangoPoseData pose) {
        StringBuilder stringBuilder = new StringBuilder();

        float translation[] = pose.getTranslationAsFloats();
        stringBuilder.append("Position: " +
                translation[0] + ", " + translation[1] + ", " + translation[2]);

        float orientation[] = pose.getRotationAsFloats();
        stringBuilder.append(". Orientation: " +
                orientation[0] + ", " + orientation[1] + ", " +
                orientation[2] + ", " + orientation[3]);

        Log.i(TAG, stringBuilder.toString());
    }

}
